# Groupe :
Robin Chappuis
Julien Dufourt


# Lien du jeu de données
https://www.data.gouv.fr/fr/datasets/base-de-donnees-accidents-corporels-de-la-circulation/#_


# Questions proposées
- Quels sont les principaux facteurs d'accidents de la route en France ?
- Quels sont les conséquences des accidents en France ?

# Question retenue
- La ceinture de sécurité diminue-t-elle la gravité des accidents ?

# Notes :
Dans notre jeu de données, les colonnes qui nous intéresserons sont celles du port de la ceinture ou non et la gravité des blessures des victimes.