Author : Robin Chappuis
Title : Names popularity comparison between 'Louis' and 'Jean'
Date : February 2020

# Dowloading and preparing file
# Source : Insee 2018 - birth stats
```{r}
file = "dpt2018_txt.zip"
if(!file.exists(file)){
  download.file("https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2018_csv.zip",
	destfile=file)
}
unzip(file)

```

# Building the dataframe from file
```{r}
library(tidyverse)
df <- read_delim("dpt2018_txt.zip",delim=";")
head(df)
```
# Placing every 'JEAN' in variable jean
```{r}
jean<-df %>%
  #Removing XXXX values
  filter(annais!="XXXX") %>%
  #Transforming years from String into Int
  mutate(annee = as.integer(annais)) %>%
  select(-annais) %>%
  
  filter(preusuel == "JEAN") %>%
  group_by(preusuel,annee) %>%
  summarise(somme = sum(nombre))
  
  head(jean)
```
# Placing every 'LOUIS' in variable louis
```{r}
louis<-df %>%
  filter(preusuel == "LOUIS") %>%
  group_by(preusuel,annais) %>%
  summarise(somme = sum(nombre)) %>%
  filter(annais!="XXXX") %>%
  mutate(annee = as.integer(annais)) %>%
  select(-annais)
  head(louis)
```
# Showing both births of Jean and Louis
```{r}
comparison = rbind(jean,louis)
comparison %>%
  ggplot() +
    aes(x=annee) +
    aes(y=somme) +
    aes(color = preusuel) +
    scale_color_manual(values = c("#8888ff","#88ddff")) +
    geom_point(size=0.9) +
    geom_line(size=0.1) +
    xlab("Année de naissance") +
    ylab("Nombre de naissances")
```
