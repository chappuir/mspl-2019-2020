---
title: "Gravité des accidents de la route en fonction du port de la ceinture"
author: "Robin Chappuis - Julien Dufourt"
date: "Mars 2020"
output:
  pdf_document: default
---

# Sommaire
- Introduction
  - Contexte
  - Problématique
- Méthodologie
  - Procédures de nettoyage des données
  - Scientific Workflow
  - Choix de représentation des données
- Analyse en programmation lettrée
- Conclusion
- Références

# Introduction

## Contexte

La base de données utilisée par ce projet est intitulée "Accidents corporels de la circulation" et provient du Ministère de l'Intérieur. Elle est disponible en suivant le lien ci-dessous :

https://www.data.gouv.fr/fr/datasets/base-de-donnees-accidents-corporels-de-la-circulation/#

Les données contenus dans cette base sont saisies lors d'un accident de voiture par les représentants des forces de l'ordre au moment de l'intervention.

La base de données définit un "Accident Corporel de la circulation" comme :

- Un accident ayant eu lieu sur une voie (publique ou privée) ouverte à la circulation publique
- Impliquant au moins un véhicule
- Impliquant au moins une personne ayant nécessité des soins

**Ainsi, il est important de noter que les accidents n'ayant pas nécessité l'intervention des urgences, et notamment les accidents n'ayant fait aucun blessé, ne sont pas présents dans cette base de données.**

La base de données est séparée en 4 tables :

- Vehicules (Les informations sur les véhicules impliqués dans l'accident)
- Usagers (Les informations concernant les personnes impliqués dans l'accident)
- Lieux (Les informations sur le lieux de l'accident)
- Caractéristiques (Autres informations à propos de l'accident)

(Nous n'utiliserons que les 2 premières tables pour notre analyse.)

Toutes ces tables possèdent une colonne "Num_Acc" (pour "Numéro d'accident"), permettant de relier les informations sur un accident d'une table à une autre.

## Problématique

L'objectif de ce projet est de répondre à la problématique suivante :

**"La ceinture de sécurité permet-elle de réduire la gravité des accidents de la circulation ?"**

Nous allons pour cela utiliser les définitions fournies par la base de données.

- **Indemne** : Personne impliquée dans l'accident mais ne nécessitant aucun soin
- **Blessé léger** : Personne ayant eu recours à des soins médicaux et n'ayant pas été admise à l'hôpital plus de 24h.
- **Blessé grave** : Appelé également blessé hospitalisé, personne ayant eu recours à des soins et présente à l'hôpital plus de 24h.
- **Tués** : Personne décédée du fait de l'accident, sur le coup ou dans les 30 jours suivants

Les deux colonnes principales permettant de répondre à la problématique sont :

- "grav" : La gravité de l'accident
- "secu" : Le matériel de sécurité présent et/ou utilisé, entre autre les ceintures de sécurité

# Méthodologie

## Procédures de nettoyage des données

Nos analyses seront centrées sur la comparaison entre les usagers portant une ceinture de sécurité et ceux n'en portant pas. Pour cela, il est nécéssaire de filtrer tous les usagers n'ayant pas accès à une ceinture de sécurité (piétons, bicyclettes, cyclomoteurs...) et ceux dont l'information n'a pas été recueilli par le représentant des forces de l'ordre.

Nous souhaitons déterminer un "pourcentage de fidélité" : l'effectif utilisé dans nos analyses par rapport à l'effectif total des usagers concernés par notre étude.

Afin de calculer ces effectifs, nous allons répartir les données en quatre catégories, qui sont :

- "Ceintures renseignées" : Les usagers dont on connait le port de la ceinture ou non (données utilisés par nos analyses, compris dans notre échantillon)
- "Ceintures disponibles mais non renseignées" : Les usagers qui avaient une ceinture de sécurité à disposition, mais dont on ne connait pas son utilisation (compris dans notre échantillon mais non-utilisé)
- "Pas de ceinture dans véhicule sans ceinture" : Les usagers qui n'avaient pas de ceinture à disposition et n'étaient pas dans un véhicule possédant des ceintures (hors échantillon)
- "Pas de ceinture dans véhicule à ceinture" : Les usagers qui n'avaient pas de ceinture à disposition mais étaient dans des véhicules possédant des ceintures (compris dans notre échantillon mais non-utilisé)

Le calcul de notre pourcentage de fidélité sera le suivant :

**100 * Ceintures renseignées / (Ceintures renseignées + Ceintures disponibles mais non renseignées + Pas de ceinture dans véhicule avec ceinture)**

Nous rappelons une nouvelle fois que notre échantillon ne comprend pas les accidents n'ayant pas fait de blessés, ces derniers n'étant par reccueillis dans la base de données utilisée.

Pour nos deux dernières catégories, le choix des véhicules considérés comme "à ceinture" ou "sans ceinture" est subjectif. Nous avons considérés comme "véhicule à ceinture" les véhicules suivantes :

- Voiturette
- Véhicules Légers
- Véhicules Utilitaires
- Poids Lourds
- Tracteurs (routiers et agricoles)
- Engins spéciaux
- Autocar

Et les "véhicules sans ceintures" les véhicules suivants :

- Bicyclettes
- Cyclomoteurs
- Scooters
- Motocyclettes
- Quads (légers et lourds)
- Trains
- Tramways
- Autobus
- Autres véhicules

## Scientific Workflow

![Workflow](workflow.jpg)

## Choix de représentation des données

Le but est d'observer la différence de gravité entre les accidents avec ceinture et les accidents sans ceinture. Comme le nombre d'accidents de ces deux catégories sont très différents, nous utiliserons principalement les pourcentages.

Pour cela, nous allons tout d'abord représenter le taux de gravité des accidents dans un pannel de deux histogrammes, "avec ceinture" et "sans ceinture".

Nous réaliserons ensuite un histogramme empilé, en réunissant les valeurs des deux précédents graphiques en un seul, afin de faciliter la comparaison entre les deux.

# Analyse en programmation lettrée

```{r initialisation}
#-----Initialisation-----



#Importation des librairies
suppressMessages(library(tidyverse))

#Téléchargement de la base de données usagers-2018
file = "usagers-2018.csv"
if(!file.exists(file)){
  download.file(
    "https://www.data.gouv.fr/fr/datasets/r/72b251e1-d5e1-4c46-a1c2-c65f1b26549a",
	  destfile=file)
}
usagers <- read_delim("usagers-2018.csv",delim=",",col_types = cols())

#Téléchargement de la base de données vehicules-2018
file = "vehicules-2018.csv"
if(!file.exists(file)){
  download.file(
    "https://www.data.gouv.fr/fr/datasets/r/b4aaeede-1a80-4d76-8f97-543dad479167",
	  destfile=file)
}
vehicules <- read_delim("vehicules-2018.csv",delim=",",col_types = cols())

```

```{r nettoyage_donnees}
#-----Nettoyage des données-----



#On fait le produit cartésien des tables usagers et véhicules
rawdata <- merge(usagers,vehicules)         

#Nombre total d'usagers :
nbUsagers = nrow(usagers) 



#On détermine les usagers ayant une ceinture à disposition
dataCeintureDisponible <- rawdata %>%
  filter(secu=="11" | secu=="12" | secu=="13" | secu=="1")

#Nombre d'usagers avec une ceinture de sécurité à disposition :
nbCeintureDisponible = nrow(dataCeintureDisponible)



#On détermine notre catégorie "Ceinture renseignée".
#Cette catégorie deviendra notre jeu de données pour les analyses.
dataCeintureDisponible <- dataCeintureDisponible %>%
  filter(secu=="11" | secu=="12")

#Nombre d'usagers de notre catégorie "Ceinture renseignée"
nbCeintureRenseignee = nrow(dataCeintureDisponible)

#Nombre d'usagers de notre catégorie "Ceinture disponible mais non renseignée"
nbCeintureDisponibleNonRenseignee = nbCeintureDisponible - nbCeintureRenseignee



#On regarde les usagers n'ayant pas de ceinture à disposition
dataCeintureNonDisponible <- rawdata %>%
  filter(!(secu=="11" | secu=="12" | secu=="13" | secu=="1") | is.na(secu))

#Nombre d'usagers sans ceinture de sécurité à disposition :
nbCeintureNonDisponible = nrow(dataCeintureNonDisponible)



#On détermine notre catégorie "Pas de ceinture dans véhicule avec ceinture"
dataCeintureNonDisponible <- dataCeintureNonDisponible %>%
filter(catv=="03" | catv=="07"
       | catv=="10" | catv=="13"
       | catv=="14" | catv=="15"
       | catv=="16" | catv=="17"
       | catv=="20" | catv=="21"
       | catv=="38")

#Nombre d'usagers de notre catégorie "Pas de ceinture dans véhicule avec ceinture"
nbPasCeintureVehiculeAvecCeinture = nrow(dataCeintureNonDisponible)

#Nombre d'usagers de notre catégorie "Pas de ceinture dans véhicule sans ceinture"
nbPasCeintureVehiculeSansCeinture =
  nbCeintureNonDisponible - nbPasCeintureVehiculeAvecCeinture



#Affichage des effectifs de nos 4 catégories :
cat("Effectif 'Ceinture renseignée' : ",
    nbCeintureRenseignee, "\n")
cat("Effectif 'Ceinture disponible non renseignée' : ",
    nbCeintureDisponibleNonRenseignee, "\n")
cat("Effectif 'Ceinture indisponible dans véhicule sans ceinture' : ",
    nbPasCeintureVehiculeSansCeinture, "\n")
cat("Effectif 'Ceinture indisponible dans véhicule avec ceinture' : ",
    nbPasCeintureVehiculeAvecCeinture, "\n")



#Calcul du pourcentage de fidélité :
fidelite = 100 * nbCeintureRenseignee /
    (nbCeintureRenseignee +
     nbCeintureDisponibleNonRenseignee +
     nbPasCeintureVehiculeAvecCeinture)
cat("Le jeu de données que nous utiliserons est fidèle à ",
    fidelite, "% de l'échantillon.")



#Notre catégorie "Ceinture renseignéee" contient toutes les lignes
#que nous souhaitons utiliser. Nous allons maintenant faire quelques
#dernières modifications pour obtenir les colonnes souhaitées.
cleandata <- dataCeintureDisponible %>%
  mutate(grav = as.character(grav)) %>% #Colonne "Gravite" convertir de number à string
  mutate(ceinture = as.character(secu))

#On remplace les valeurs numériques de "grav" par leur signification
cleandata$grav[cleandata$grav=="1"] = "Indemne"
cleandata$grav[cleandata$grav=="2"] = "Tué"
cleandata$grav[cleandata$grav=="3"] = "Blessé léger"
cleandata$grav[cleandata$grav=="4"] = "Blessé grave"

#On remplace les valeurs numériques de "secu" par leur signification
cleandata$ceinture[cleandata$secu=="11"] = "oui"
cleandata$ceinture[cleandata$secu=="12"] = "non"

#On ne conserve que les colonnes utiles à notre traitement des données
cleandata <- cleandata %>% select(Num_Acc,grav,ceinture) 
head(cleandata)
```

```{r preparation_donnees}
#-----Préparation des données-----

#Calcul du nombre d'accidents avec et sans ceinture
nbAccidentsAvecCeinture = nrow(cleandata%>%filter(ceinture=="oui"))
nbAccidentsSansCeinture = nrow(cleandata%>%filter(ceinture=="non"))



#Création d'un dataset des accidents avec ceinture uniquement
accidentsAvecCeinture <- cleandata %>%
  group_by(grav,ceinture) %>%     #Regroupement par gravité et ceinture
  count() %>%
  filter(ceinture=="oui") %>%     #Filtrage des ceintures
  mutate(taux = as.double(n))     #Création de la colonne "taux"

#Calcul des taux
accidentsAvecCeinture$taux =
  100*accidentsAvecCeinture$n/nbAccidentsAvecCeinture 
accidentsAvecCeinture$taux =
  round(accidentsAvecCeinture$taux, digits = 1)



#Création d'un dataset des accidents sans ceinture uniquement
accidentsSansCeinture <- cleandata %>%
  group_by(grav,ceinture) %>%     #Regroupement par gravité et ceinture
  count() %>%
  filter(ceinture=="non") %>%     #Filtrage des ceintures
  mutate(taux = as.double(n))     #Création de la colonne "taux"

#Calcul des taux
accidentsSansCeinture$taux =
  100*accidentsSansCeinture$n/nbAccidentsSansCeinture 
accidentsSansCeinture$taux =
  round(accidentsSansCeinture$taux, digits = 1)



#Dataset regroupant les deux autres
accidentsRegroupes <- rbind(accidentsAvecCeinture, accidentsSansCeinture)
rm(accidentsAvecCeinture,accidentsSansCeinture)



#Aperçu du tableau de données que nous allons utiliser dans les graphiques
accidentsRegroupes

#Note : Nous avons besoin de le faire en deux temps dans deux datasets différents,
#car les calculs des taux n'utilise pas les mêmes nombres.

```
```{r construction_graphiques}
#-----Construction des graphiques-----



#Utlisation de "factors" pour ordonner les données dans nos graphiques
accidentsRegroupes$ceinture_f = factor(accidentsRegroupes$ceinture,
  levels=c("oui","non"))
accidentsRegroupes$grav_f = factor(accidentsRegroupes$grav,
  levels=c("Tué","Blessé grave","Blessé léger","Indemne"))



#Panneaux d'histogrammes des accidents
#en fonction de leur gravité et du port de la ceinture
ggplot(data=accidentsRegroupes, aes(x=grav_f,y=taux,fill=grav_f)) +
  ggtitle("Gravité des accidents en fonction du port de la ceinture") +
  xlab("Port de la ceinture") +
  ylab("Taux d'accidents") +
  ylim(0, 100) +
  theme_light()+
  theme(legend.position = "none") +
  scale_fill_manual(values=c("#e06666","#f6b26b","#ffd966","#6d9eeb")) +
  guides(fill=guide_legend(title="Conséquence de l'accident")) +
  geom_bar(stat = "identity", width=0.3) +
  geom_text(aes(label=taux), vjust=-0.3, size=3.5) +
  facet_grid(. ~ ceinture_f)



#Histogramme empilé des accidents
#en fonction de leur gravité et du port de la ceinture
ggplot(data=accidentsRegroupes, aes(x=ceinture_f,y=taux,fill=grav_f)) +
  ggtitle("Gravité des accidents en fonction du port de la ceinture") +
  xlab("Port de la ceinture") +
  ylab("Gravité de l'accident") +
  coord_flip() +
  theme_light()+
  geom_bar(stat = "identity", width=0.3, color="black") +
  guides(fill=guide_legend(title="Conséquence de l'accident")) +
  scale_fill_manual(values=c("#e06666","#f6b26b","#ffd966","#6d9eeb"))

```

# Conclusion

On observe qu'un utilisateur portant sa ceinture augmente de 70% ses chances de rester indemne en cas d'accident comparé à un utilisateur ne la portant pas.

Aussi, une personne ne portant pas de ceinture multiplie par x8,8 ses chances de décès en cas d'accident.


On peut donc en conclure que oui, le port de la ceinture de sécurité réduit la gravité des accidents.

# Références

- Lien vers le site de la base de données (data.gouv.fr) :

https://www.data.gouv.fr/fr/datasets/base-de-donnees-accidents-corporels-de-la-circulation/#

- Lien de téléchargement des fichiers csv utilisés dans ce projet :
  - usagers-2018 : https://www.data.gouv.fr/fr/datasets/r/72b251e1-d5e1-4c46-a1c2-c65f1b26549a
  - vehicules-2018 : https://www.data.gouv.fr/fr/datasets/r/b4aaeede-1a80-4d76-8f97-543dad479167


